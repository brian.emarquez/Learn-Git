<h2 align="center">Learn Git &nbsp;:heart:&nbsp;</h2>

<p align="center">
  
  <a href="https://github.com/BrianMarquez3/Learn-Git/tags">
    <img src="https://img.shields.io/github/tag/BrianMarquez3/Learn-Git.svg?label=version&style=flat" alt="Version">
  </a>
  <a href="https://github.com/BrianMarquez3/Learn-Git/stargazers">
    <img src="https://img.shields.io/github/stars/BrianMarquez3/Learn-Git.svg?style=flat" alt="Stars">
  </a>
  <a href="https://github.com/BrianMarquez3/Learn-Git/network">
    <img src="https://img.shields.io/github/forks/BrianMarquez3/Learn-Git.svg?style=flat" alt="Forks">
  </a>

</p>
  
![git](./images/clientes-git-gui.jpg)

## ¿Que es Git?
es un software de control de versiones diseñado por Linus Torvalds, pensando en la eficiencia y la confiabilidad del mantenimiento de versiones de aplicaciones cuando éstas tienen un gran número de archivos de código fuente. Su propósito es llevar registro de los cambios en archivos de computadora y coordinar el trabajo que varias personas realizan sobre archivos compartidos. [WIKIPEDIA](https://es.wikipedia.org/wiki/Git).<br>

## Instalación

📦 [Install Git](https://git-scm.com/) Instalador de Git.<br>
📦 [Install Github](https://desktop.github.com/) Instalador de Github.<br>

## Book

📦 [Descargar Libro de Git](https://git-scm.com/book/en/v2) Descargar Book


